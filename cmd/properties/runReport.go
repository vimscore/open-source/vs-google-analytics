package properties

import (
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	analyticsdata "google.golang.org/api/analyticsdata/v1beta"
	"log"
	"strings"
	"vimscore.com/vs-google-analytics/internal"
)

var runReport = struct {
	property string
	start    string
	end      string

	metrics    []string
	dimensions []string
	orderBys   []string
}{}

func runRunReport(*cobra.Command, []string) error {
	if err := internal.InitGlobals(); err != nil {
		return err
	}
	svc, err := internal.NewService()
	if err != nil {
		return err
	}

	ranges := []*analyticsdata.DateRange{{
		StartDate: runReport.start,
		EndDate:   runReport.end,
	}}

	req := analyticsdata.RunReportRequest{
		DateRanges: ranges,
	}

	if len(runReport.metrics) == 0 {
		return errors.New("at least one metric is required")
	}

	for _, dimension := range runReport.dimensions {
		req.Dimensions = append(req.Dimensions, &analyticsdata.Dimension{
			Name: dimension,
		})
	}

	for _, orderBy := range runReport.orderBys {
		var desc bool
		if strings.HasSuffix(orderBy, "-") {
			desc = true
			orderBy = strings.TrimSuffix(orderBy, "-")
		} else if strings.HasSuffix(orderBy, "+") {
			desc = false
			orderBy = strings.TrimSuffix(orderBy, "+")
		}

		log.Printf("desc: %v\n", desc)

		var dimension *analyticsdata.DimensionOrderBy
		for _, s := range runReport.dimensions {
			if orderBy == s {
				dimension = &analyticsdata.DimensionOrderBy{
					DimensionName: orderBy,
				}
			}
		}

		var metric *analyticsdata.MetricOrderBy
		for _, s := range runReport.metrics {
			if orderBy == s {
				metric = &analyticsdata.MetricOrderBy{
					MetricName: orderBy,
				}
			}
		}

		req.OrderBys = append(req.OrderBys, &analyticsdata.OrderBy{
			Desc:      desc,
			Dimension: dimension,
			Metric:    metric,
		})
	}

	var header []string
	var data [][]string

	batchSize := 10
	for batch := 0; batch < len(runReport.metrics); batch += batchSize {
		req.Metrics = nil
		end := batch + batchSize
		if end > len(runReport.metrics) {
			end = len(runReport.metrics)
		}

		log.Printf("Metric batch %d, %d metrics\n", batch/batchSize, end-batch)
		for _, metric := range runReport.metrics[batch:end] {
			req.Metrics = append(req.Metrics, &analyticsdata.Metric{
				Name: metric,
			})
		}

		path := fmt.Sprintf("properties/%s", runReport.property)
		res, err := svc.Properties.RunReport(path, &req).Do()
		if err != nil {
			return err
		}

		logHeaders(res)

		for _, s := range res.MetricHeaders {
			header = append(header, s.Name)
		}

		for _, s := range res.DimensionHeaders {
			header = append(header, s.Name)
		}

		for rowIdx, row := range res.Rows {
			var r []string

			if batch > 0 {
				r = data[rowIdx]
			}

			logRow(row)

			for _, value := range row.MetricValues {
				r = append(r, value.Value)
			}

			for _, value := range row.DimensionValues {
				r = append(r, value.Value)
			}

			if batch == 0 {
				data = append(data, r)
			} else {
				data[rowIdx] = r
			}
		}
	}

	err = internal.WriteOutput("", header, data)

	return nil
}

func logRow(row *analyticsdata.Row) {
	log.Printf("Metric values:\n")
	for _, value := range row.MetricValues {
		log.Printf("value.Value: %s\n", value.Value)
	}

	log.Printf("Dimension values:\n")
	for _, value := range row.DimensionValues {
		log.Printf("value.Value: %s", value.Value)
	}
}

func logHeaders(res *analyticsdata.RunReportResponse) {
	var s = ""
	for _, h := range res.MetricHeaders {
		s += " " + h.Name
	}
	log.Printf("Headers:%s\n", s)
}

func init() {
	c := &cobra.Command{Use: "runReport", RunE: runRunReport}
	propertiesCmd.AddCommand(c)

	flags := c.Flags()
	flags.SortFlags = false

	flags.StringVar(&runReport.property, "property", "", "The Analytics Property ID")
	_ = c.MarkFlagRequired("property")

	flags.StringVar(&runReport.start, "start", "", "Start of date. Either a date formatted as YYYY-MM-DD, 'today', 'yesterday' or 'NdaysAgo'")
	_ = c.MarkFlagRequired("start")

	flags.StringVar(&runReport.end, "end", "", "End of date. Either a date formatted as YYYY-MM-DD, 'today', 'yesterday' or 'NdaysAgo'")
	_ = c.MarkFlagRequired("end")

	flags.StringArrayVar(&runReport.metrics, "metric", nil, "A metric to request. Can be repeated")

	flags.StringArrayVar(&runReport.dimensions, "dimension", nil, "A dimension to request. Can be repeated")

	flags.StringArrayVar(&runReport.orderBys, "order-by", nil, "Name fields to order by. Suffix with '+' or '-' for ascending or descending order.")

	c.Long = `Executes the Properties.runReport call.

If more than 10 metrics are requested, multiple calls will be performed because of the API's lmits.`
	c.Example = strings.TrimSpace(`
vs-google-analytics properties runReport --property $MY_PROPERTY --start 7daysAgo --end today

vs-google-analytics properties --property $MY_PROPERTY runReport --start 7daysAgo --end today \
  --metric active1DayUsers --metric active7DayUsers --metric active28DayUsers \
  --dimension country --dimension date+
`)
}
