package properties

import (
	"github.com/spf13/cobra"
	"vimscore.com/vs-google-analytics/internal"
)

var propertiesCmd = &cobra.Command{
	Use: "properties",
}

func init() {
	internal.RootCmd.AddCommand(propertiesCmd)
}
