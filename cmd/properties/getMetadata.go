package properties

import (
	"fmt"
	"github.com/spf13/cobra"
	"vimscore.com/vs-google-analytics/internal"
)

var getMetadata = struct {
	property string
}{}

func runGetMetadata(*cobra.Command, []string) error {
	if err := internal.InitGlobals(); err != nil {
		return err
	}

	svc, err := internal.NewService()
	if err != nil {
		return err
	}

	path := fmt.Sprintf("properties/%s/metadata", getMetadata.property)
	res, err := svc.Properties.GetMetadata(path).Do()
	if err != nil {
		return err
	}

	var data [][]string
	header := []string{
		"API Name",
		"UI Name",
		"Type",
		"Custom Definition",
		"Expression",
		"Description",
	}

	for _, metric := range res.Metrics {
		data = append(data, []string{
			metric.ApiName,
			metric.UiName,
			metric.Type,
			fmt.Sprintf("%v", metric.CustomDefinition),
			metric.Expression,
			metric.Description,
		})
	}

	if err = internal.WriteOutput("Metrics", header, data); err != nil {
		return err
	}

	data = nil
	header = []string{
		"API Name",
		"UI Name",
		"Description",
	}

	for _, dimension := range res.Dimensions {
		data = append(data, []string{
			dimension.ApiName,
			dimension.UiName,
			dimension.Description,
		})
	}

	return internal.WriteOutput("Dimensions", header, data)
}

func init() {
	c := &cobra.Command{Use: "getMetadata", RunE: runGetMetadata}
	propertiesCmd.AddCommand(c)

	c.Flags().StringVar(&getMetadata.property, "property", "", "The Analytics Property ID")
	_ = c.MarkFlagRequired("property")
}
