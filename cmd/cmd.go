package cmd

import (
	"github.com/spf13/cobra"
	"vimscore.com/vs-google-analytics/internal"

	// Make sure all commands are initialized
	_ "vimscore.com/vs-google-analytics/cmd/properties"
)

func RootCmd() *cobra.Command {
	return internal.RootCmd
}
