package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/cobra/doc"
	"log"
	"os"
	"testing"
)

var rootDir = "../doc"

func Test_GenerateMarkdown(t *testing.T) {
	fix(RootCmd())

	markdownDirName := mkdir(t, "cmd")
	err := doc.GenMarkdownTree(RootCmd(), markdownDirName)
	if err != nil {
		log.Fatal(err)
	}
}

func fix(cmd *cobra.Command) {
	cmd.DisableAutoGenTag = true
	for _, child := range cmd.Commands() {
		fix(child)
	}
}

func mkdir(t *testing.T, name string) string {
	dirName := rootDir + "/" + name

	err := os.RemoveAll(dirName)
	if err != nil {
		t.Error(err)
	}

	err = os.MkdirAll(dirName, os.ModePerm)
	if err != nil {
		t.Error(err)
	}

	return dirName
}
