module vimscore.com/vs-google-analytics

go 1.16

require (
	github.com/spf13/cobra v1.2.1
	google.golang.org/api v0.44.0
	github.com/olekukonko/tablewriter v0.0.5
	//google.golang.org/api/analyticsdata/v1beta v0.60.0
)
