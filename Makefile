BIN=vs-google-analytics
GOSRC=$(shell find . -name \*.go)
export MAKEFLAGS=-r

all: $(BIN) test
.PHONY: all

bin: $(BIN)
.PHONY: bin

junk:
	echo GOSRC=$(GOSRC)

$(BIN): $(GOSRC)
	bin/vs-ga-build "$(BIN)"

test:
	go test ./...

clean:
	rm -f $(wildcard $(BIN)*)
