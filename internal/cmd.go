package internal

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
	"strconv"
	"vimscore.com/vs-google-analytics/build"
)

const CmdName = "vs-google-analytics"

var RootCmd = &cobra.Command{Use: CmdName, SilenceUsage: true}
var Args RootArgs

var state = make(map[string]string)

type RootArgs struct {
	Format  string
	Verbose bool
}

func (args *RootArgs) Get(cmd *cobra.Command, flagName string, varName string, required bool) (value string) {
	flag := cmd.Flag(flagName)
	if flag.Changed {
		value = flag.Value.String()
	}

	if value == "" {
		value = state[varName]
	}

	if value == "" && required {
		_, _ = fmt.Fprintln(os.Stderr, "'"+flagName+"' is required. Either specify --"+flagName+" or put it in the state file")
		os.Exit(1)
	}

	return value
}

func (args *RootArgs) GetInt64(cmd *cobra.Command, flagName string, varName string, required bool) (value int64) {
	s := args.Get(cmd, flagName, varName, required)

	if s == "" {
		return 0
	}

	value, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		_, _ = fmt.Fprintln(os.Stderr, "'"+flagName+"' is must be an integer")
		os.Exit(1)
	}

	return
}

var completionCmd = &cobra.Command{
	Use:   "completion",
	Short: "Generates bash completion scripts",
	Long: `To load completion run

. <(` + CmdName + ` completion)

To configure your bash shell to load completions for each session add to your bashrc

# ~/.bashrc or ~/.profile
. <(` + CmdName + ` completion)
`,
	Run: func(cmd *cobra.Command, args []string) {
		err := RootCmd.GenBashCompletion(os.Stdout)
		if err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "%v", err)
		}
	},
}

func init() {
	var v string
	//goland:noinspection GoBoolExpressions
	if len(build.Version) > 0 {
		v = build.Version
	} else {
		v = "local"

		if len(build.Timestamp) > 0 {
			v += ", timestamp=" + build.Timestamp
		}

		if len(build.GitShortSha) > 0 {
			v += ", git=" + build.GitShortSha
		}
	}

	RootCmd.Version = v

	RootCmd.PersistentFlags().BoolVarP(&Args.Verbose, "verbose", "v", false,
		"Enabled verbose output")

	RootCmd.PersistentFlags().StringVar(&Args.Format, "format", "pretty",
		"Output format, one of: pretty, csv, json")
	RootCmd.Flags().SortFlags = false

	RootCmd.AddCommand(completionCmd)
}
