package internal

import (
	"context"
	"fmt"
	"google.golang.org/api/analyticsdata/v1beta"
	"io/ioutil"
	"log"
)

func InitGlobals() error {
	if !Args.Verbose {
		log.SetOutput(ioutil.Discard)
	}

	return nil
}

func NewService() (svc *analyticsdata.Service, err error) {
	svc, err = analyticsdata.NewService(context.Background())
	if err != nil {
		err = fmt.Errorf("could not create analytics data service: %w", err)
	}

	return
}
