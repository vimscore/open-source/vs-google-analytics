package internal

import (
	"encoding/csv"
	"fmt"
	"github.com/olekukonko/tablewriter"
	"os"
)

func WriteOutput(title string, header []string, data [][]string) error {
	if Args.Format == "csv" {
		w := csv.NewWriter(os.Stdout)
		err := w.Write(header)
		if err != nil {
			return err
		}

		err = w.WriteAll(data)
		if err != nil {
			return err
		}
	} else {
		if len(title) > 0 {
			fmt.Println(title)
		}
		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader(header)
		table.AppendBulk(data)
		table.Render()
	}

	return nil
}
