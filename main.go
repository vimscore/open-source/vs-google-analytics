package main

import (
	"os"
	"vimscore.com/vs-google-analytics/cmd"
)

func main() {
	if err := cmd.RootCmd().Execute(); err != nil {
		os.Exit(1)
	}
}
