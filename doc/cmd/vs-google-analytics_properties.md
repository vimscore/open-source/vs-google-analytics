## vs-google-analytics properties



### Options

```
  -h, --help   help for properties
```

### Options inherited from parent commands

```
      --format string   Output format, one of: pretty, csv, json (default "pretty")
  -v, --verbose         Enabled verbose output
```

### SEE ALSO

* [vs-google-analytics](vs-google-analytics.md)	 - 
* [vs-google-analytics properties getMetadata](vs-google-analytics_properties_getMetadata.md)	 - 
* [vs-google-analytics properties runReport](vs-google-analytics_properties_runReport.md)	 - 

