## vs-google-analytics properties getMetadata



```
vs-google-analytics properties getMetadata [flags]
```

### Options

```
  -h, --help              help for getMetadata
      --property string   The Analytics Property ID
```

### Options inherited from parent commands

```
      --format string   Output format, one of: pretty, csv, json (default "pretty")
  -v, --verbose         Enabled verbose output
```

### SEE ALSO

* [vs-google-analytics properties](vs-google-analytics_properties.md)	 - 

