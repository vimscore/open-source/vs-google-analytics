## vs-google-analytics properties runReport



### Synopsis

Executes the Properties.runReport call.

If more than 10 metrics are requested, multiple calls will be performed because of the API's lmits.

```
vs-google-analytics properties runReport [flags]
```

### Examples

```
vs-google-analytics properties runReport --property $MY_PROPERTY --start 7daysAgo --end today

vs-google-analytics properties --property $MY_PROPERTY runReport --start 7daysAgo --end today \
  --metric active1DayUsers --metric active7DayUsers --metric active28DayUsers \
  --dimension country --dimension date+
```

### Options

```
      --property string         The Analytics Property ID
      --start string            Start of date. Either a date formatted as YYYY-MM-DD, 'today', 'yesterday' or 'NdaysAgo'
      --end string              End of date. Either a date formatted as YYYY-MM-DD, 'today', 'yesterday' or 'NdaysAgo'
      --metric stringArray      A metric to request. Can be repeated
      --dimension stringArray   A dimension to request. Can be repeated
      --order-by stringArray    Name fields to order by. Suffix with '+' or '-' for ascending or descending order.
  -h, --help                    help for runReport
```

### Options inherited from parent commands

```
      --format string   Output format, one of: pretty, csv, json (default "pretty")
  -v, --verbose         Enabled verbose output
```

### SEE ALSO

* [vs-google-analytics properties](vs-google-analytics_properties.md)	 - 

