## vs-google-analytics



### Options

```
      --format string   Output format, one of: pretty, csv, json (default "pretty")
  -v, --verbose         Enabled verbose output
  -h, --help            help for vs-google-analytics
```

### SEE ALSO

* [vs-google-analytics completion](vs-google-analytics_completion.md)	 - Generates bash completion scripts
* [vs-google-analytics properties](vs-google-analytics_properties.md)	 - 

