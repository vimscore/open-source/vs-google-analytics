## vs-google-analytics completion

Generates bash completion scripts

### Synopsis

To load completion run

. <(vs-google-analytics completion)

To configure your bash shell to load completions for each session add to your bashrc

# ~/.bashrc or ~/.profile
. <(vs-google-analytics completion)


```
vs-google-analytics completion [flags]
```

### Options

```
  -h, --help   help for completion
```

### Options inherited from parent commands

```
      --format string   Output format, one of: pretty, csv, json (default "pretty")
  -v, --verbose         Enabled verbose output
```

### SEE ALSO

* [vs-google-analytics](vs-google-analytics.md)	 - 

